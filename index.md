---
layout: 000-main
title: Прагматичный гайд по управлению знаниями
---

<div class="pt-4 pb-3">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1 class="">Прагматичный гайд по Управлению Знаниями</h1>
    </div>
  </div>
</div>
</div>
<div class="pt-3 pb-3">
<div class="container">
  <div class="row">
    <div class="col-md-12 border-left border-warning">
      <h2 class=""><small class="text-muted"><a href="/usage/README.html">Как войти в тему Управления Знаниями</a> и начать применять лучшие практики в своей команде</small></h2>
      <p class="text-monospace">Мы создаём понятный пошаговый гайд, который позволит не-специалистам войти в тему управления знаниями и сразу начать применять лучшие практики в своей команде, а специалистам — систематизировать свои знания и изучить существующие практики, чтобы набраться идей и вдохновения для новых свершений.</p>
    </div>
  </div>
</div>
</div>
<div class="pt-3  pb-3">
<div class="container">
  <div class="row">
    <div class="col-md-12 border-left border-warning">
      <h2 class="text-left"><small class="text-muted">Тематики</small></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 border-left border-warning">
      <p class="lead"><a href="/wins/README.html">Запуск управления знаниями "На минималках"</a></p>
      <p class="">Как начать смотреть на свою компанию с точки зрения управления знаниями и суметь найти "Быстрые выигрыши" — дешёвые с точки зрения затрат, но выгодные для компании, изменения.</p>
    </div>
    <div class="col-md-6">
      <p class="lead"><a href="/practices/onboarding/README.html">Практики Онбординга</a></p>
      <p class="">Систематизированные и собранные в одном месте основные практики онбординга, с их преимуществами и недостатками (использование handbook, использование чек-листов, наставничество, обучающие курсы и другие).</p>
    </div>
    <div class="col-md-6 border-warning border-left">
      <p class="lead"><a href="/practices/knowledge-registration/README.html">Практики для фиксации знаний</a></p>
      <p class="">Систематизированные и собранные в одном месте практики, связанные с фиксацией знаний в виде текста/схем/баз данных, со всеми их преимуществами, недостатками и трудностями внедрения.</p>
    </div>
    <div class="col-md-6">
      <p class="lead"><a href="/practices/knowledge-sharing/README.html">Культура обмена знаниями</a></p>
      <p class="">Как привить команде и компании практики управления знаниями, которые нужно использовать в своей повседневной жизни.</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 border-left border-warning">
      <p class="lead">Выученные уроки и лучшие практики (в разработке)</p>
      <p class="">Как научить команду и компанию выносить уроки из получаемого опыта и формировать лучшие практики</p>
    </div>
    <div class="col-md-6">
      <p class="lead">Картирование знаний (в разработке)</p>
      <p class="">Как начать смотреть на свою компанию с точки зрения управления знаниями и суметь найти "Быстрые выигрыши" — дешёвые с точки зрения затрат, но выгодные для компании, изменения.</p>
    </div>
    <div class="col-md-6 border-left border-warning">
      <p class="lead">Практики Knowledge Manager-ов (в разработке)</p>
      <p class="">Какие роли есть у людей, занимающихся Knowledge Management-ом в компании, какие цели перед этими людьми ставятся и как оценить результаты их работы</p>
    </div>
    <div class="col-md-6">
      <p class="lead" contenteditable="true">Knowledge Centered Service (в разработке)</p>
      <p class="">Организация технической поддержки с использованием практик Управления Знаниями</p>
    </div>
  </div>
</div>
</div>
<div class="pt-3 pb-3">
<div class="container">
  <div class="row">
    <div class="col-md-12 border-left border-warning">
      <h2 class="text-left">
        <font color="#6c757d"><span style="font-size: 25.6px;">Как использовать</span></font>
      </h2>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 border-left border-warning">
      <p class="">
        <b>Если вы уже знакомы с темой и терминами управления знаний — рекомендуем сразу перейти к изучению практик в <a href="/practices/README.html">разделе "Практики"</a>. В нём собрано описание процессов и культурных элементов. Часть из них скорее всего уже есть в вашей компании, но вы наверняка почерпнёте для себя что-то новое.<br>
        Если вы видите много незнакомых терминов и задаётесь вопросом "зачем это мне" — лучше начать с <a href="/wins/README.html">раздела "Быстрые выигрыши"</a>. В этом разделе мы стараемся собрать самые дешёвые с точки зрения затрат, но эффективные приёмы управления знаниями.</b>
      </p>
      <!--
      <img class="img-fluid d-block" src="../../Desktop/Снимок экрана 2020-06-16 в 21.58.42.png">
      -->
      <p class="">Гайд — это живая база знаний, она постоянно пополняется. Редакция периодически проводит сессии обмена знаниями и, вообще говоря, является работающим профессиональным сообществом. Мы будем <a href="/usage/CONTRIBUTE.html">рады вашему участию</a> в любом виде.</p>
    </div>
  </div>
</div>
</div>
