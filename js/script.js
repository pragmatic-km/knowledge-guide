const nav = document.querySelector('.js-practice-nav');
const navItems = nav.querySelectorAll('.practice-nav__parent');

navItems.forEach(item => {
    item.addEventListener('click', (e) => {
        item.parentNode.classList.toggle('practice-nav__item--active')
    });
})